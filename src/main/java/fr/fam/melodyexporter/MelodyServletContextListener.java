package fr.fam.melodyexporter;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import fr.fam.melodyexporter.config.MelodyConfig;

/**
*
*/
@WebListener
public class MelodyServletContextListener implements ServletContextListener {

    /** */
    private static final Logger LOGGER = Logger.getLogger(MelodyServletContextListener.class);

    @Override
    public final void contextInitialized(final ServletContextEvent sce) {

        MelodyConfig config = new MelodyConfig();
        MelodyCollector collector = new MelodyCollector(config);

        ServletContext sc = sce.getServletContext();
        sc.setAttribute("collector", collector);

        LOGGER.info("ServletContextListener started");
    }

    @Override
    public final void contextDestroyed(final ServletContextEvent sce) {

        ServletContext sc = sce.getServletContext();
        MelodyCollector collector = (MelodyCollector) sc.getAttribute("collector");
        collector.close();
        sc.removeAttribute("collector");

        LOGGER.info("ServletContextListener destroyed");
    }
}
