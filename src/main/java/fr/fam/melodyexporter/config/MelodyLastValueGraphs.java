package fr.fam.melodyexporter.config;

/**
*
*/
public enum MelodyLastValueGraphs {

    // Host Metrics
    /** */
    USED_PHYSICAL_MEMORY_SIZE("usedPhysicalMemorySize"),
    /** */
    USED_SWAP_SPACE_SIZE("usedSwapSpaceSize"),
    /** */
    SYSTEM_LOAD("systemLoad"),
    /** */
    SYSTEM_CPU_LOAD("systemCpuLoad"),
    /** */
    OPEN_FILES("fileDescriptors"),

    // JVM Metrics
    /** */
    USED_MEMORY("usedMemory"),
    /** */
    USED_BUFFERED_MEMORY("usedBufferedMemory"),
    /** */
    USED_NON_HEAP_MEMORY("usedNonHeapMemory"),
    /** */
    LOADED_CLASSES_COUNT("loadedClassesCount"),
    /** */
    GARBAGE_COLLECTOR("gc"),
    /** */
    CPU("cpu"),
    /** */
    THREAD_COUNT("threadCount"),
    /** */
    ACTIVE_THREADS("activeThreads"),

    // Basic HTTP Metrics
    /** */
    HTTP_SESSIONS("httpSessions"),
    /** */
    HTTP_SESSIONS_MEAN_AGE("httpSessionsMeanAge"),
    /** */
    ACTIVE_CONNECTIONS("activeConnections"),
    /** */
    USED_CONNECTIONS("usedConnections"),

    // HTTP Metrics
    /** */
    HTTP_HITS_RATE("httpHitsRate"),
    /** */
    HTTP_MEAN_TIMES("httpMeanTimes"),
    /** */
    HTTP_SYSTEM_ERRORS("httpSystemErrors"),

    // SQL Metrics
    /** */
    SQL_HITS_RATE("sqlHitsRate"),
    /** */
    SQL_MEAN_TIMES("sqlMeanTimes"),
    /** */
    SQL_SYSTEM_ERRORS("sqlSystemErrors"),

    // EJB Metrics
    /** */
    EJB_HITS_RATE("ejbHitsRate"),
    /** */
    EJB_MEAN_TIMES("ejbMeanTimes"),
    /** */
    EJB_SYSTEM_ERRORS("ejbSystemErrors"),

    // SPRING Metrics
    /** */
    SPRING_HITS_RATE("springHitsRate"),
    /** */
    SPRING_MEAN_TIMES("springMeanTimes"),
    /** */
    SPRING_SYSTEM_ERRORS("springSystemErrors"),

    // GUICE Metrics
    /** */
    GUICE_HITS_RATE("guiceHitsRate"),
    /** */
    GUICE_MEAN_TIMES("guiceMeanTimes"),
    /** */
    GUICE_SYSTEM_ERRORS("guiceSystemErrors"),

    // JSP Metrics
    /** */
    JSP_HITS_RATE("jspHitsRate"),
    /** */
    JSP_MEAN_TIMES("jspMeanTimes"),
    /** */
    JSP_SYSTEM_ERRORS("jspSystemErrors"),

    // Jenkins Metrics
    /** */
    RUNNING_BUILDS("runningBuilds"),
    /** */
    BUILD_QUEUE_LENGTH("buildQueueLength"),

    // Tomcat Metrics
    /** */
    TOMCAT_BUSY_THREADS("tomcatBusyThreads"),
    /** */
    TOMCAT_BYTES_RECEIVED("tomcatBytesReceived"),
    /** */
    TOMCAT_BYTES_SENT("tomcatBytesSent");


    /** */
    private String parameterName;

    /**
    *
    * @param pparameterName parameterName
    */
    MelodyLastValueGraphs(final String pparameterName) {
        parameterName = pparameterName;
    }

    /**
    *
    * @return parameterName
    */
    public String getParameterName() {
        return parameterName;
    }
}
