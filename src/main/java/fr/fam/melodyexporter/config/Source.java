package fr.fam.melodyexporter.config;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
*
*/
public class Source {
    /** */
    static final int DEFAULT_TIMEOUT = 5000;

    /** */
    private String url;

    /** */
    private int timeout = DEFAULT_TIMEOUT;

    /** */
    private String login;

    /** */
    private String password;

    /** */
    private Map<String, String> labels = new HashMap<String, String>();

    /** */
    private List<Metric> metrics = new ArrayList<Metric>();

    /**
    *
    * @return url
    */
    public final String getUrl() {
        return url;
    }

    /**
    *
    * @param purl application url
    */
    public final void setUrl(final String purl) {
        url = purl;
    }

    /**
    *
    * @return timeout
    */
    public final int getTimeout() {
        return timeout;
    }

    /**
    *
    * @param ptimeout timeout
    */
    public final void setTimeout(final int ptimeout) {
        timeout = ptimeout;
    }

    /**
    *
    * @return login
    */
    public final String getLogin() {
        return login;
    }

    /**
    *
    * @param plogin application login
    */
    public final void setLogin(final String plogin) {
        login = plogin;
    }

    /**
    *
    * @return password
    */
    public final String getPassword() {
        return password;
    }

    /**
    *
    * @param ppassword application password
    */
    public final void setPassword(final String ppassword) {
        password = ppassword;
    }

    /**
    *
    * @return labels
    */
    public final Map<String, String> getLabels() {
        return labels;
    }

    /**
    *
    * @param plabels application labels
    */
    public final void setLabels(final Map<String, String> plabels) {
        labels = plabels;
    }

    /**
    *
    * @return metrics
    */
    public final List<Metric> getMetrics() {
        return metrics;
    }

    /**
    *
    * @param pmetrics application metrics
    */
    public final void setMetrics(final List<Metric> pmetrics) {
        metrics = pmetrics;
    }

    /**
    *
    * @param plabels plabels
    */
    public final void forwardLabels(final Map<String, String> plabels) {
        labels.putAll(plabels);
        labels.put("source", url);
        for (Metric m : getMetrics()) {
            m.addLabels(labels);
        }
    }

    /**
    *
    * @param pmetrics metrics
    */
    public final void forwardMetrics(final List<Metric> pmetrics) {
        for (Metric m : pmetrics) {
            // metrics will have different labels, we have to duplicate them
            Metric nm = new Metric();
            nm.setName(m.getName());
            nm.setLabels(new HashMap<String, String>(m.getLabels()));
            nm.setValue(m.getValue());
            metrics.add(nm);
        }
    }

    /**
    *
    * @return string
    */
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("url=" + url + ", ");
        s.append("timeout=" + timeout + ", ");
        s.append("login=" + login + ", ");
        s.append("password=" + "*******" + ", ");

        // browsing labels
        s.append("labels=[");

        for (Map.Entry<String, String> e : labels.entrySet()) {
            s.append("{");
            s.append(e.getKey());
            s.append(", ");
            s.append(e.getValue());
            s.append("}, ");
        }
        s.append("], ");

        // browsing metrics
        s.append("metrics=[");
        for (Metric m : metrics) {
            s.append(m.toString());
            s.append(", ");
        }
        s.append("]");

        return s.toString();
    }
}
