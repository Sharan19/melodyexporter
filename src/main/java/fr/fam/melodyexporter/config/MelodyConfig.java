package fr.fam.melodyexporter.config;

import java.io.InputStream;
import java.util.Scanner;

import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import org.apache.log4j.Logger;

/**
*
*/
public class MelodyConfig {

    /** */
    private static final Logger LOGGER = Logger.getLogger(MelodyConfig.class);

    /** */
    private static final String SETTINGS_FILENAME = "melodyexporter.yml";

    /** */
    private static Applications applications;

    /** */
    private static MultiMap<String, Metric> finalMetrics;

    /**
    *
    * @throws IllegalStateException IllegalStateException
    */
    public MelodyConfig() throws IllegalStateException {
        LOGGER.debug("Reading configuration...");

        try {
            // Get applications file and open it
            InputStream settingsInputStream = Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream(SETTINGS_FILENAME);

            // Parse settings file
            LOGGER.debug("Parsing yaml file...");

            // load applications
            Yaml yamlApplications = new Yaml(new Constructor(Applications.class));
            applications = (Applications) yamlApplications.load(yamlRead(settingsInputStream));
            LOGGER.info("Applications loaded : " + applications.getApplications().size());

            // check config integrity
            checkApplications();

            // consolidate metrics
            consolidateMetrics();

            LOGGER.debug("Applications : " + applications.toString());

        } catch (Exception e) {
                LOGGER.error("Can't read settings file : " + SETTINGS_FILENAME);
                LOGGER.error("Exception : ", e);
                throw new IllegalStateException("Can't read settings file");
        }
    }

    /**
    *
    * @return applications
    */

    public final Applications getApplications() {
        return applications;
    }

    /**
    *
    * @return finalMetrics
    */

    public final MultiMap<String, Metric> getFinalMetrics() {
        return finalMetrics;
    }

    /**
    * Read the yaml documents into string.
    *
    * @param in inputStream
    * @return string
    */
    private String yamlRead(final InputStream in) {
        Scanner s = new Scanner(in).useDelimiter("\\A");

        String buffer;
        if (s.hasNext()) {
            buffer = s.next();
        } else {
            buffer = "";
        }

        return ("applications:\n" + buffer);
    }

    /**
    *
    * @throws IllegalStateException IllegalStateException
    */
    private void checkApplications() throws IllegalStateException {
        LOGGER.info("Checking config...");

        // for each application
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                // check url
                if (source.getUrl() == null) {
                    LOGGER.error("Missing url for application : " + application.getName());
                    throw new IllegalStateException("Missing url for application : " + application.getName());
                }

                // check metrics
                for (Metric m : source.getMetrics()) {
                    Boolean known = false;
                    for (MelodyLastValueGraphs g : MelodyLastValueGraphs.values()) {
                        if (g.getParameterName().equals(m.getName())) {
                            known = true;
                        }
                    }
                    if (!known) {
                        LOGGER.error("Unknown metric : " + application.getName() + "." + m.getName());
                        throw new IllegalStateException("Unknown metric : " + application.getName() + "." + m.getName());
                    }
                }
            }
        }
        LOGGER.info("Done.");

    }

    /**
    *
    */
    private void consolidateMetrics() {
        LOGGER.info("Consolidate metrics...");

        // forward application metrics to sources, duplicates are ignored
        for (Application application : applications.getApplications()) {
            application.forwardMetrics();
        }

        // forward application labels to sources, duplicates are overrided
        for (Application application : applications.getApplications()) {
            application.forwardLabels();
        }

        // build the metric list
        finalMetrics = new MultiMap<String, Metric>();

        // for each app
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                // for each metric
                for (Metric m : source.getMetrics()) {
                    // add the metric
                    finalMetrics.putOne(application.getName() + "_" + m.getName(), m);
                }

            }

        }

        // check for missing labels in metrics and complete
        // if needed. Same metrics must have the same label keys

        // for each metric
        for (Map.Entry<String, List<Metric>> m : finalMetrics.entrySet()) {

            Set<String> labelKeys = new HashSet<String>();

            // for each value
            // get all labels keys
            for (Metric v : m.getValue()) {

                // get all metric labels keys
                for (Map.Entry<String, String> e : v.getLabels().entrySet()) {
                    labelKeys.add(e.getKey());
                }

            }

            // for each value
            // check for missing label keys and add if needed
            for (Metric v : m.getValue()) {

                // for each label key
                for (String k : labelKeys) {
                    if (!v.getLabels().containsKey(k)) {
                        v.getLabels().put(k, "missing");
                    }
                }

            }

        }

        LOGGER.info("Done.");
    }

}
