package fr.fam.melodyexporter.config;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
*
*/
public class Application {
    /** */
    private String name;

    /** */
    private Map<String, String> labels = new HashMap<String, String>();

    /** */
    private List<Metric> metrics = new ArrayList<Metric>();

    /** */
    private List<Source> sources = new ArrayList<Source>();

    /**
    *
    * @return name
    */
    public final String getName() {
        return name;
    }

    /**
    *
    * @param pname application name
    */
    public final void setName(final String pname) {
        name = pname;
    }

    /**
    *
    * @return labels
    */
    public final Map<String, String> getLabels() {
        return labels;
    }

    /**
    *
    * @param plabels application labels
    */
    public final void setLabels(final Map<String, String> plabels) {
        labels = plabels;
    }

    /**
    *
    * @return metrics
    */
    public final List<Metric> getMetrics() {
        return metrics;
    }

    /**
    *
    * @param pmetrics application metrics
    */
    public final void setMetrics(final List<Metric> pmetrics) {
        metrics = pmetrics;
    }

    /**
    *
    * @return sources
    */
    public final List<Source> getSources() {
        return sources;
    }

    /**
    *
    * @param psources sources
    */
    public final void setSources(final List<Source> psources) {
        sources = psources;
    }

    /**
    *
    */
    public final void forwardLabels() {
        for (Source source : getSources()) {
            source.forwardLabels(labels);
        }
    }

    /**
    *
    */
    public final void forwardMetrics() {
        for (Source source : getSources()) {
            source.forwardMetrics(metrics);
        }
    }

    /**
    *
    * @return string
    */
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("{name=" + name + ", ");

        // browsing labels
        s.append("labels=[");
        for (Map.Entry<String, String> e : labels.entrySet()) {
            s.append("{");
            s.append(e.getKey());
            s.append(", ");
            s.append(e.getValue());
            s.append("}, ");
        }
        s.append("], ");

        // browsing sources
        s.append("Sources[");
        for (Source a : sources) {
            s.append(a.toString());
            s.append(", ");
        }

        s.append("]");

        s.append("}");

        return s.toString();
    }

}
